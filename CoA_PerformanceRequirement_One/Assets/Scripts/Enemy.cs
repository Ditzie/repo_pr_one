﻿using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        private int currentHealth;

        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        [SerializeField]
        private int attackAmount;

        public int AttackAmount
        {
            get { return attackAmount; }
            set { attackAmount = value; }
        }

        [SerializeField]
        private bool isAlive;

        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }
        public void Damage(int dmgAmount)
        {
           if(CurrentHealth > 0)
            {
              CurrentHealth = CurrentHealth - dmgAmount;

                if(CurrentHealth <=0)
                {
                    CurrentHealth = 0;
                    isAlive = false;

                    Log(EntityName+" Hp is " + CurrentHealth);
                    return;

                }

        
            } 

           if(CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                isAlive = false;

                Log(EntityName+" Hp is" + CurrentHealth);
                return;
            }

            Log(EntityName+" has been damaged! Its Hp is now " + CurrentHealth);
        }
    }
}