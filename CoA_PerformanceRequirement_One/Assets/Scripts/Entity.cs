﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Entity : MonoBehaviour, ILog
    {
        [SerializeField]
        private string entityName;

        public string EntityName
        {
            get {return entityName; }
            set { entityName = value; }
        }

        public void Log(object loggable)
        {
            Debug.Log(loggable);
        }
    }

}
