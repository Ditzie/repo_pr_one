﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public Entity[] entities;

        public void Log(object loggable)
        {
            Debug.Log(loggable);
        }
        void Start()
        {
            Log("Names are registered.");

            for (int i = 0; i < entities.Length; i++)
            {
                Log("You are at index " + i + ". Name of the Entity is " + entities[i].EntityName);
            }
        }



    }
}
