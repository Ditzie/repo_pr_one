﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity,IAttack
    {
        [SerializeField]
        private int currentHealth;
        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        [SerializeField]
        private int attackAmount;
        public int AttackAmount
        {
            get{ return attackAmount; }
            set{ attackAmount = value; }
        }

        [SerializeField]
        private bool isAlive;
        public bool IsAlive
        {
            get { return isAlive;}
            set { isAlive = value; }
        }

        [SerializeField]
        private Enemy enemyEntity;
        public Enemy EnemyEntity
        {
            get { return enemyEntity; }
            set { enemyEntity = value; }
        }

        public void AttackEnemy(Enemy enemy, int atkDmg)
        {
           if(enemy.IsAlive == true)
            {
                enemy.Damage(atkDmg);
            }
        }

        void Start()    
        {
            Log(this.EntityName);
            Log(currentHealth);
            Log(AttackAmount);
            AttackEnemy(enemyEntity,attackAmount);
        }
    }

}